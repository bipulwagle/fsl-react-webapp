// Created By Bipul Wagle.
// Gateway into the Application. Handles login and session login.

import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Login from './components/Login';
import SignUp from './components/SignUp';
import HousesStore from './stores/HousesStore';

import LoggedIn from './components/LoggedIn';
import AlertContainer from 'react-alert';


class App extends Component {
  alertOptions = {
    offset: 14,
    position: 'bottom left',
    theme: 'dark',
    time: 5000,
    transition: 'scale'
  }
 
  showAlert(message = "Testing Message!", type = 'success', duration = 2000){
    this.msg.show(message, {
      time: duration,
      type: type
    })
  }

  constructor(props){
    super(props);
    // console.log("dkjflsadjfas;fdjsfa;sf  ffaklsdfasl;fasdf ajflaja",this.props);
    this.msg = null;
    this.state = {
      loggedIn : false
    };
    this.changeEventHandler = this.changeEventHandler.bind(this);
    this.showAlert = this.showAlert.bind(this);
  }



  componentWillMount(){
    HousesStore.on("change", this.changeEventHandler);
  }

  componentWillUnmount(){
    HousesStore.removeListener("change", this.changeEventHandler);
  }

  //On Housestore change see if use is logged in, set state accordingly.
  changeEventHandler(){
    // console.log(HousesStore.currentToken);
    console.log("change event handler app js");
    var tempState = this.state;
    tempState.loggedIn = HousesStore.isLoggedIn();
    this.setState(tempState);
  }

  // Login and SignUp are visible when user is not logged in, once logged in the LoggedIn component is visible.
  render() {
    return (
      <div className="App">
        <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
        <div className="LoginContainer">
          {!this.state.loggedIn ? <Login showAlert = {(message, type, duration) => this.showAlert(message, type, duration)}/> : null}
          {!this.state.loggedIn ? <SignUp showAlert = {(message, type, duration) => this.showAlert(message, type, duration)}/> : null}
          {this.state.loggedIn ?  <LoggedIn showAlert = {(message, type, duration) => this.showAlert(message, type, duration)}/> : null}       
        </div>
      </div>
    );
  }
}

export default App;
