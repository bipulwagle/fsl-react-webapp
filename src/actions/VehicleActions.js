// Created By Bipul Wagle;
// Flux Actions on the Household VehiclesStore, e.g. ServerCalls
import dispatcher from "../dispatcher";
import *  as validationUtil from "../lib/ValidationUtil"
import HouseStore from "../stores/HousesStore"
import * as NetworkConfig from "../lib/NetworkConfig"

var axios = require('axios');

var baseApiEndpoint = NetworkConfig.backendEndpoint;

// USED as keys for the dispatcher and store to communicate.
export const actions = {
    create: "HOUSEVEHICLES_CREATE",
    fetch: "HOUSEVEHICLES_FETCH"
}


//Does basic validation on the passed vehicle object(json object). Makes call to the server to create vehicle for the logged in household.
//CurrentToken determines logged in status.
//vehicle should include memberId associated with the vehicle.
//Check to make sure server responds with 200. Server responses and errors, are passed down stores.
export function createVehicle(vehicle){
    var inptoken = HouseStore.currentToken;
    if(!inptoken){
        dispatcher.dispatch({ type: actions.create, response: { error:true, error_msg:"Not Logged In"}})
    }
    var params = ["memberId", "make", "model", "year", "license_plate"];

    validationUtil.basicObjValidation(params, vehicle). then(valid=>{
        var serverParams = {
            token : inptoken,
            data: vehicle
        }
        axios.post(baseApiEndpoint+"/api/vehicles", serverParams).then(onResolved=>{
            switch(onResolved.status){
                case 200:{
                    dispatcher.dispatch({ type: actions.create, response: onResolved.data });
                    break;
                }
                default:{
                    dispatcher.dispatch( { type:actions.create, response: { error:true, error_msg:"Server Error" }});
                    break;
                }
            }
        }, onRejected=>{
            dispatcher.dispatch( {
                type: actions.create, 
                response:{
                    error: true,
                    error_msg: "Network Error"
                } 
            });
        });
    }, invalid=>{
        dispatcher.dispatch({ type:actions.create, response:invalid });
    })
}


//Makes call to the server to get all vehicles for the logged in household.
//CurrentToken determines logged in status.
//token is appended to the url
//Server responses and errors, are passed down stores.
export function fetchVehicles(){
    var token = HouseStore.currentToken;
    if(!token){
        dispatcher.dispatch({ type:actions.fetch, response:{ error:true, error_msg:"Not Logged In" }});
    }else{
        axios.get(baseApiEndpoint+"/api/vehicles?token="+token).then(onResolved=>{
                dispatcher.dispatch({type: actions.fetch, response: onResolved.data});
        }, onRejected=>{
                dispatcher.dispatch({ type: actions.fetch, response: {error: true, error_msg: "Server Error"}});
        })
    }
}