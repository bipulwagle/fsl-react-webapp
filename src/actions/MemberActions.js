// Created By Bipul Wagle;
// Flux Actions on the Household MembersStore, e.g. ServerCall 

import dispatcher from "../dispatcher";
import * as validationUtil from "../lib/ValidationUtil"
import HouseStore from "../stores/HousesStore"
import * as NetworkConfig from "../lib/NetworkConfig"

var axios = require('axios');

var baseApiEndPoint = NetworkConfig.backendEndpoint;


// USED as keys for the dispatcher and store to communicate.
export const actions = {
    create: "HOUSEMEMBERS_CREATE",
    fetch: "HOUSEMEMBERS_FETCH",
}


//Does basic validation on the passed members object(json object). Makes call to the server to create member for the logged in household.
//CurrentToken determines logged in status.
//Check to make sure server responds with 200. Server responses and errors, are passed down stores.
export function createMembers(member){
    var inptoken = HouseStore.currentToken;
    if(!inptoken){
        dispatcher.dispatch({ type: actions.create, response: { error:true, error_msg:"Not Logged In"}})
    }    
    var params = ["first_name", "last_name", "email", "age", "gender"]

    validationUtil.basicObjValidation(params, member).then(valid=>{
        var serverParams = {
            token: inptoken,
            data: member
        }
        axios.post(baseApiEndPoint+"/api/members", serverParams).then(onResolved=>{
            console.log("create member: ", onResolved.data);
            // console.log("create members response: ", onResolved.data.data);
            switch(onResolved.status){
                case 200:{
                    dispatcher.dispatch({ type:actions.create, response: onResolved.data});
                    
                    break;
                }
                default:{
                    dispatcher.dispatch({ type:actions.create, response:{ error:true, error_msg:"Server Error" }});
                    break;
                }
            }
        }, onRejected=>{
            dispatcher.dispatch({
                type: actions.create,
                response:{
                    error: true,
                    error_msg: "Network Error"
                }
            });
        });
    }, invalid=>{
        dispatcher.dispatch({ type:actions.create, response:invalid})
    });

}

//Makes call to the server to get all member for the logged in household.
//CurrentToken determines logged in status.
//token is appended to the url
//Server responses and errors, are passed down stores.
export function fetchMembers(){
    var token = HouseStore.currentToken;
    if(!token){
        dispatcher.dispatch({ type:actions.fetch, response:{ error:true, error_msg:"Not Logged In" }});
    }else{
        axios.get(baseApiEndPoint+"/api/members?token="+token).then(onResolved=>{
            dispatcher.dispatch({ type:actions.fetch, response: onResolved.data});
        }, onRejected=>{
            dispatcher.dispatch({ type:actions.fetch, response:{ error:true, error_msg:"Server Error"}});
        });
    }
}

