// Created By Bipul Wagle;
// Flux Actions on the HouseStore, e.g. ServerCall 

import dispatcher from "../dispatcher";
import * as validationUtil from "../lib/ValidationUtil"
import * as NetworkConfig from "../lib/NetworkConfig"

var axios = require('axios');

var baseApiEndpoint = NetworkConfig.backendEndpoint;



// USED as keys for the dispatcher and store to communicate.
export const actions = {
    login:"HOUSESTORE_LOGIN",
    create: "HOUSESTORE_CREATE",

}

//Does basic validation on the passed house object(json object). Makes call to the server to sign in.
//Check to make sure server responds with 200. Server responses and errors, are passed down stores.
export function loginHouse(house){

        var params = ["address", "password"];

        validationUtil.basicObjValidation(params, house).then(valid=>{
            axios.post(baseApiEndpoint+"/signin", house).then(onResolved=>{
                switch(onResolved.status){
                    case 200:{
                        console.log("sign in ok");
                        dispatcher.dispatch({ type: actions.login, response: onResolved.data });
                        break;
                    }

                    default:{
                        dispatcher.dispatch( { type: actions.login, response: {
                            error: true,
                            error_msg: "Server Error"
                        }});
                        break;
                    }
                }
            }, onRejected=>{
                dispatcher.dispatch( {
                    type: actions.login, 
                    response:{
                        error: true,
                        error_msg: "Network Error"
                    } 
                });
            });
        }, 
            invalid=>{
                dispatcher.dispatch({
                    type: actions.login,
                    response: invalid
            });
        } );
}


//Does basic validation on the passed house object(json object). Makes call to the server to createHouse("user").
//Check to make sure server responds with 200. Server responses and errors, are passed down to stores.

export function createHouse(house){

    var params = ["address", "password", "zip", "city", "state", "number_of_bedrooms"];
    console.log(params);
    validationUtil.basicObjValidation(params, house).then(valid=>{
        axios.post(baseApiEndpoint+"/houses", house).then(onResolved=>{
            switch(onResolved.status){
                case 200:{
                    dispatcher.dispatch( { type: actions.create, response: onResolved.data } );
                    break;
                }

                default:{
                    dispatcher.dispatch( { type: actions.create, response: {
                        error: true,
                        error_msg: "Server Error"
                    }});
                    break;
                }
            }
        }, onRejected=>{
            dispatcher.dispatch( {
                type: actions.create, 
                response:{
                    error: true,
                    error_msg: "Network Error"
                } 
            });
        })
    }, invalid=>{
       dispatcher.dispatch({
           type: actions.create,
           response: invalid
       });
    });
}
