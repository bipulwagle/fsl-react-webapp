// Created By Bipul Wagle
// Validation Utils Library.

// used to do (if exists) validation on object's members.
// @params  keys : List of strings containing objects keys to check.
// @params obs  : Object to validate

export function basicObjValidation(keys,obj){
    return new Promise(function(resolve, reject){
        for (var element of keys){
            if(!obj[element]){
                reject({
                    error: true,
                    error_msg: element + " is missing!"
                });
                break;
            }
        }
        resolve();
    });
    
}