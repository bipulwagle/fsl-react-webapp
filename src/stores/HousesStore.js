// Created By Bipul Wagle;
// Flux Stores for the House Model.

import { EventEmitter } from "events";
import  dispatcher from "../dispatcher";
import  { actions as HouseStoreActions } from "../actions/HouseActions";


class HouseStore extends EventEmitter{
    constructor(){
        super();
        this.currentHouse = {};
        this.currentToken = null;
    }
    
    getcurrentHouse(){
        return this.currentHouse
    }

    
    isLoggedIn(){
        if(this.currentToken){
            return true;
        }else{
            return false;
        }
    }

    //Handler to Log house ("user") in by setting the currentHouse and currentToken variables in this store.
    //emits -  change to listeners to notify of the store change.
    //emits -  login with success or error response.
    loginHouseHandler(response){
        if(!response){
            this.emit("login",{
                error: true,
                error_msg: "Server Did not Respond"
            });
            return;
        }

        if(response.error){
            this.emit("login",response);
            return
        }

        console.log("login handler: ", response);
        this.currentToken = response.token;
        this.currentHouse = response.data;
        this.emit("login",{error: false});
        this.emit("change");
    }
    
    //Handles action's dispatches, calls appropriate handler within the store.
    //If the user is logged in or created the server responds in the same response format. 
    //Therefore, we handle both create response and login response with same handler
    handleDispatchActions(action){
        console.log(action);
        switch(action.type){
            case HouseStoreActions.login:{
                this.loginHouseHandler(action.response);
                break;
            }

            case HouseStoreActions.create:{
                this.loginHouseHandler(action.response);
                break;
            }
            
        }
    }

}

//creates a singleton houseStore and registers it to listen to actions.
const houseStore = new HouseStore;
dispatcher.register(houseStore.handleDispatchActions.bind(houseStore));

export default houseStore;