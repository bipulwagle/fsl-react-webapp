// Created By Bipul Wagle;
// Flux Stores for the House Members Model.


import { EventEmitter } from "events";
import dispatcher from "../dispatcher";
import { actions as MemberStoreActions } from "../actions/MemberActions";
import * as MemberActions from "../actions/MemberActions";
import * as VehicleActions from "../actions/VehicleActions";

class MemberStore extends EventEmitter{
    constructor(){
        super();
        this.members = [];
    }

    getMembers(){
        return this.members;
    }

    // Sets stores members to the members passed in from response.
    // emits - fetch with success or error response.
    // emits - change to notify of store changes.

    setMembers(response){

        if(!response){
            this.emit("fetch", { error:true, error_msg:"Server Did not Respond" });
            return;
        }
        if(response.error){
            this.emit("fetch", response);
            return;
        }
       
        this.members = response.data;
        console.log(this.members);
        this.emit("fetch", {error: false});
        this.emit("change"); 
    }

    //Handles create member's response.
    // emits - create with sucess or response.
    createMemberActionHandler(response){
        if(!response){
            this.emit("create", { error:true, error_msg:"Server Did not Respond"});
            return;
        }
        console.log("#####++++++######", this.members);
        this.emit("create",response);
        MemberActions.fetchMembers();
        VehicleActions.fetchVehicles();

        return;
    }

    // Handles action's dispatches, calls appropritate handler withing this store.
    handleDispatchActions(action){
        switch(action.type){
            case MemberStoreActions.create:{
                this.createMemberActionHandler(action.response);
                break;
            }

            case MemberStoreActions.fetch:{
                this.setMembers(action.response);
                break;
            }
        }
    }

}
//createa a singleton houseStore and registers it to listen to actions.
const memberStore = new MemberStore;
dispatcher.register(memberStore.handleDispatchActions.bind(memberStore));

export default memberStore;