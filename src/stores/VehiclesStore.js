// Created By Bipul Wagle;
// Flux Store For House Vehicles Model.

import { EventEmitter } from "events";
import dispatcher from "../dispatcher";
import { actions as VehicleStoreActions } from "../actions/VehicleActions";
import * as MemberActions from "../actions/MemberActions";
import * as VehicleActions from "../actions/VehicleActions";

class VehicleStore extends EventEmitter{
    constructor(){
        super();
        this.vehicles = [];
    }

    getVehicles(){
        return this.vehicles;
    }

    
    getVehiclesForMember(member){
        if(member){
            var resultVar = [];
            var curr_vehicles = this.vehicles;
            for(var vehicle of curr_vehicles){
                if(vehicle.memberId == member.id){
                    resultVar.push(vehicle);
                }
            }
            return resultVar;
        }
        return [];
    }

    // Sets stores vehicles to the vehicles passed in from response.
    // emits - fetch with success or error response.
    // emits - change to notify of store changes.

    setVehicles(response){

        if(!response){
            this.emit("fetch", { error:true, error_msg:"Server Did not Respond"} );
            return;
        }

        if(response.error){
            this.emit("fetch", response);
            return;
        }

        this.vehicles = response.data;
        this.emit("fetch", {error: false});
        this.emit("change");
    }

    //Handles create vehicle's response.
    // emits - create with sucess or response.
    createVehicleActionHandler(response){
        if(!response){
            this.emit("create", { error:true, error_msg:"Server Did not Respond"} );
            return;
        }

        this.emit("create", {error:false});
        MemberActions.fetchMembers();
        VehicleActions.fetchVehicles();
        return;

    }

    // Handles action's dispatches, calls appropritate handler withing this store.
    handleDispatchActions(action){
        switch(action.type){
            case VehicleStoreActions.create:{
                this.createVehicleActionHandler(action.response);
                break;
            }
            case VehicleStoreActions.fetch:{
                this.setVehicles(action.response);
                break;
            }
        }
    }
}

//createa a singleton houseStore and registers it to listen to actions.

const vehicleStore = new VehicleStore;
dispatcher.register(vehicleStore.handleDispatchActions.bind(vehicleStore));

export default vehicleStore; 