// Created By Bipul Wagle;
// AddVehicle Form Component

import React, { Component } from 'react';
import VehicleStore from "../stores/VehiclesStore";
import MemberStore from "../stores/MembersStore";
import * as VehiclesAction from "../actions/VehicleActions";
import * as MembersAction from "../actions/MemberActions";

class AddVehicle extends Component{

    //sets up bindings and state from the stores.
    //
    constructor(){
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleMemberSelection = this.handleMemberSelection.bind(this);
        this.generateMemberOptionsJSX = this.generateMemberOptionsJSX.bind(this);
        this.vehicleStoreCreate = this.vehicleStoreCreate.bind(this);

        this.state = {
            vehicles : VehicleStore.getVehicles(),
            members: MemberStore.getMembers(),
            selectedMember: null
        };
        
        
    }

    //handles form submit, creates vehicle object an passes it to VechilesAction.createVehicle.
    handleSubmit(e){
        e.preventDefault();
        if(this.state.selectedMember){
            var input_vehicle = {
                memberId: this.state.selectedMember.id,
                make: this.refs.make.value,
                model: this.refs.model.value,
                year: this.refs.year.value,
                license_plate: this.refs.year.value
            }
            VehiclesAction.createVehicle(input_vehicle);
        }
        
    }

    //adding and removing store listeners
    componentWillMount(){
        VehicleStore.on("create", this.vehicleStoreCreate);
    }

    componentWillUnmount(){
        VehicleStore.removeListener("create", this.vehicleStoreCreate);
    }

    //checks if vehicle create was successfull. resseting the form on success
    vehicleStoreCreate(data){
        if(data.error){

        }else{
            this.refs.make.value = "";
            this.refs.model.value = "";
            this.refs.year.value = 2017;
            this.refs.license.value = "";
        }
    }

    //
    generateMemberOptionsJSX(){
        var memberJSXArr = [];
        memberJSXArr.push(<option value="">-</option>);
        if(this.state.members){
            for(var val of this.state.members){
                // console.log(val);
                memberJSXArr.push(<option value={val.id.toString()}>{(val.first_name+" "+val.last_name)}</option>);
            }
        }
        return memberJSXArr;
    }

    // sets the selectedMember variable in state to the selected member from the event;
    handleMemberSelection(event){
        var id = event.target.value;
        console.log("Handeling User Data Selection ", id);
        // console.log(this.state);
        var temp_state = this.state;
        for (var i of temp_state.members){
            console.log("members ", i.id);
            if(i.id == id){
                console.log(i);
                temp_state.selectedMember = i;
                break;
            }
        }
        this.setState(temp_state);
    }

    render(){
        return(
        <div className="AddVehicle">
            <br/>
            
            <div className="title" > Add Vehicle </div>

            <form onSubmit={this.handleSubmit}>
                <div className="row">
                    <div className="col">
                    <label>Member:</label>
                        <select onChange={this.handleMemberSelection}>
                            {this.generateMemberOptionsJSX()}
                            
                        </select>
                    </div>
                    <div className="col">
                    <input ref="make" type="text" className="form-control" placeholder="Vehicle Make"/>
                    </div>
                    <div className="col">
                    <input ref="model" type="text" className="form-control" placeholder="Vehicle Model"/>
                    </div>
                </div>
                <br/>
                <div className="row">
                    <div className="col">
                    <input ref="year" type="number" className="form-control" placeholder="Model Year"/>
                    </div>
                    <div className="col">
                    <input ref="license" type="text" className="form-control" placeholder="License #"/>
                    </div>
                </div>
                <br/>
                <div className="row">
                    <button type="submit" className="btn btn-primary">Add</button>
                </div>

                <br/>
            </form>

           
        </div>);
    }
}

export default AddVehicle;