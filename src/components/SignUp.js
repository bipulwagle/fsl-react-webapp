// Created By Bipul Wagle
// SignUp Form Component

import React, { Component } from 'react';
import HousesStore from "../stores/HousesStore";
import * as HouseActions from "../actions/HouseActions";

class SignUp extends Component{
    constructor(){
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    
    handleSubmit(e){
        e.preventDefault();
        var house = {
            address: this.refs.address.value,
            password: this.refs.password.value,
            zip: this.refs.zip.value,
            city: this.refs.city.value,
            state: this.refs.state.value,
            number_of_bedrooms: this.refs.number_of_bedrooms.value
        }

        HouseActions.createHouse(house);
        
    }

    render(){
        return(
            <div className = "SignUp">
                <br/>
                <div className = "title">SignUp</div>

                <form onSubmit={this.handleSubmit}>
                    <div className="form-row">
                        <div className="col-7">
                            <input ref="address" type="text" className="form-control" placeholder="Address"/>
                        </div>
                        <div className="col">
                            <input ref="password" type="password" className="form-control" placeholder="Password"/>
                        </div>
                    </div>
                    <br/>
                    <div className="form-row">
                        <div className="col-7">
                        <input ref="city" type="text" className="form-control" placeholder="City"/>
                        </div>
                        <div className="col">
                        <input ref="state" type="text" className="form-control" placeholder="State"/>
                        </div>
                        <div className="col">
                        <input ref="zip" type="text" className="form-control" placeholder="Zip"/>
                        </div>
                        <div className="col">
                        <input ref="number_of_bedrooms" type="number" className="form-control" placeholder="Bedrooms"/>
                        </div>
                    </div>
                    <br/>
                    <div className="form-row"> 
                        <button type="submit" className="btn btn-primary">Sign Up</button>
                    </div>
                </form>

                
            </div>
        );
    }

}

export default SignUp;