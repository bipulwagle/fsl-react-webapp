// Created By Bipul Wagle;
// Home Component once user is logged in.

import React, { Component } from 'react';
import HousesStore from "../stores/HousesStore";
import MembersStore from '../stores/MembersStore';
import VehiclesStore from '../stores/VehiclesStore';
import AddMember from "../components/AddMember";
import AddVehicle from "../components/AddVehicle";
import Overview from "../components/Overview";
import * as MembersAction from "../actions/MemberActions";
import * as VehiclesAction from "../actions/VehicleActions";

class LoggedIn extends Component{

    //sets up function bindings and state.
    //displayAddMember determines if AddMember component should be visible
    //displayAddVehicle determines if AddVehicle component should be visible.
    //Once house ("user") is logged in we fetch both vehicles and member associated with it.
    //this should get stored in our stores objects for their respective models.
    constructor(props){
        super(props);
        this.displayAddMemberButtonClicked = this.displayAddMemberButtonClicked.bind(this);
        this.displayAddVehicleButtonClicked = this.displayAddVehicleButtonClicked.bind(this);
        this.memberStoreOnChange = this.memberStoreOnChange.bind(this);
        this.memberStoreOnCreate = this.memberStoreOnCreate.bind(this);
        this.vehicleStoreOnCreate = this.vehicleStoreOnCreate.bind(this);
        this.state = {
            house: HousesStore.currentHouse,
            members: [],
            displayAddMember: true,
            displayAddVehicle: false
        }
        VehiclesAction.fetchVehicles();
        MembersAction.fetchMembers();
    }

    componentWillMount(){
        // DONT DO THIS this.memberStoreOnChange.bind(this)
        MembersStore.on("change", this.memberStoreOnChange);
        MembersStore.on('create', this.memberStoreOnCreate);
        VehiclesStore.on('create', this.vehicleStoreOnCreate);
    }

    componendWillUnmount(){

        MembersStore.removeListener("change", this.memberStoreOnChange);
        MembersStore.removeListener('create', this.memberStoreOnCreate);
        VehiclesStore.removeListener('create', this.vehicleStoreOnCreate);
    }

    memberStoreOnCreate(data){
        if(data.error){
            
            this.props.showAlert(data.error_msg, 'error');
        }else{
            this.props.showAlert("Member Created!");
        }
    }

    vehicleStoreOnCreate(data){
        if(data.error){
            
            this.props.showAlert(data.error_msg, 'error');
        }else{
            this.props.showAlert("Vehicle Created!");
        }
    }

    memberStoreOnChange(){
        console.log("change");
        var tempmembers = MembersStore.getMembers()
        if(tempmembers && tempmembers.length>0){
            var temp_state = this.state;
            temp_state.members = tempmembers;
            this.setState(temp_state);
            
        }else{
        }
    }

    //toggle between addmember and addvehicle
    displayAddMemberButtonClicked(e){
        var tempstate = this.state;
        tempstate.displayAddMember = true;
        tempstate.displayAddVehicle = false;
        this.setState(tempstate);
    }


    //toggle between addmember and addvehicle
    displayAddVehicleButtonClicked(e){
        var tempstate = this.state;
        tempstate.displayAddMember = false;
        tempstate.displayAddVehicle = true;
        this.setState(tempstate);
    }

    render(){
        return(
            <div className="LoggedIn">
                <br/>
                <div className="title-header">Logged In as {this.state.house.address}</div>
                <br/>
                <Overview/>
                <hr/>
                <div className="row">
                    <div className="col">
                    <button className="btn btn-primary" onClick={this.displayAddMemberButtonClicked}>Add Members</button>
                    </div>
                    
                    <div className="col">
                    <button className="btn btn-primary" onClick={this.displayAddVehicleButtonClicked}>Add Vehicle</button>
                    </div> 
                </div>
                {/* <br/> */}
                <hr/>
                {(this.state.displayAddMember) ? <AddMember/> : null}
                {(this.state.displayAddVehicle)? <AddVehicle/> : null} 
                <br/>
            </div>
        );
    }

}

export default LoggedIn;