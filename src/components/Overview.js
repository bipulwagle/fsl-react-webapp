// Created By Bipul Wagle;
// Overview display Component;

import React, { Component } from 'react';
import VehicleStore from "../stores/VehiclesStore";
import MemberStore from "../stores/MembersStore";
import * as VehiclesAction from "../actions/VehicleActions";
import * as MembersAction from "../actions/MemberActions";


class Overview extends Component{

    //set state, and bindings. 
    constructor(){
        super();
        this.vehicleStoreChange = this.vehicleStoreChange.bind(this);
        this.memberStoreChange = this.memberStoreChange.bind(this);
        this.state = {
            vehicles: [],
            members: [],
            selectedMember: null
        };
    }

    componentWillMount(){
        VehicleStore.on("change", this.vehicleStoreChange);
        MemberStore.on("change", this.memberStoreChange);
    }

    componentWillUnmount(){
        VehicleStore.removeListener("change", this.vehicleStoreChange);
        MemberStore.removeListener("change", this.memberStoreChange);
    }
    
    vehicleStoreChange(){
        var temp_vehicles = VehicleStore.getVehicles();
        if(temp_vehicles && temp_vehicles.length > 0){
            var temp_state = this.state;
            temp_state.vehicles = temp_vehicles; 
            this.setState(temp_state);
        }
    }

    memberStoreChange(){
        var temp_members = MemberStore.getMembers();
        if(temp_members && temp_members.length > 0){
            var temp_state = this.state;
            temp_state.members = temp_members;
            this.setState(temp_members);
        }
    }


    // generateVehicleJSX(){
    //     var vehicleJSXArr = [];
    //     if (this.state.vehicles){
    //         for(var val of this.state.vehicles){
    //             console.log(val);
    //             for(var item of this.state.members){
    //                 if(val.memberId == item.id ){
    //                     vehicleJSXArr.push(<div key={val.id}>
    //                         {val.make+" "+val.model+" - "+ item.first_name + " "+ item.last_name}
    //                     </div>);
    //                 }
    //             }
                
    //         }
    //     }
    //     return vehicleJSXArr;
    // }

    // generatetMemberJSX(){
    //     var membersJSXArr = [];
    //     if (this.state.members){
    //         for (var val of this.state.members){
    //             console.log(val);
                

    //             membersJSXArr.push(<div><label key={val.id}>{val.first_name} {val.last_name}</label><br/></div>);
                

    //         }
    //     }
    //     return membersJSXArr;
    // }

    generateOverviewJSX(){
        var numberOfCols = 2
        var usersJSX = this.generateUserJSX();
        var resultRows = [];
        for(var i = 0; i < this.state.members.length ; i++ ){
            if ((i % numberOfCols) == 0){
                var temp_cols = [];
                for(var j =0; j < numberOfCols; j++){
                    temp_cols.push(usersJSX.shift());
                }
                resultRows.push(<div className="row">
                    {temp_cols}
                </div>);
                resultRows.push(<br/>);
            }
        }
        return resultRows;
    }

    generateUserJSX(){
        var resultArr = [];
        for( var i = 0; i < this.state.members.length; i++){
            var vehicles_for_member1 = VehicleStore.getVehiclesForMember(this.state.members[i]);
            var vehiclesJSX = [];
            for(var item of vehicles_for_member1){
                vehiclesJSX.push(<li className="list-group-item">{item.make + " "+ item.model}</li>)
            }
            var userJSX = (
            <div class="col">
                <div className="member-title">{this.state.members[i].first_name +" "+ this.state.members[i].last_name}</div>
                <br/>
                <ul className="list-group">
                    {vehiclesJSX}
                </ul>
            </div>
            );
            resultArr.push(
                userJSX
            );
        }
        return resultArr;
    }

    render(){
        return(
        <div className="Overview"> 
            <label className="title">Members Overview</label>
            {this.generateOverviewJSX()}
        </div>
        );
    }

}

export default Overview;