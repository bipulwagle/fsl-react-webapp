// Created By Bipul Wagle;
// Login form Component;

import React, { Component } from 'react';
import HousesStore from "../stores/HousesStore";
import * as HouseActions from "../actions/HouseActions";

class Login extends Component{
    constructor(props){
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleHouseLogin = this.handleHouseLogin.bind(this);
    }

    componentWillMount(){
        // DONT DO THIS this.memberStoreOnChange.bind(this)
        
        HousesStore.on("login", this.handleHouseLogin);
    }

    componendWillUnmount(){

        HousesStore.removeListener("login", this.handleHouseLogin);
    }

    handleHouseLogin(data){

        console.log(data);
        console.log(this.props,this.state)
        if(data.error){
            
            this.props.showAlert(data.error_msg, 'error');
        }else{
            this.props.showAlert("Logged In");
        }
    }

    handleSubmit(e){
        e.preventDefault();
        HouseActions.loginHouse({
            address: this.refs.address.value,
            password: this.refs.password.value
        });

    }

    render(){
        return(
            <div className = "Login" >
                <br/>

                <form onSubmit={this.handleSubmit}>
                    <div className="row">
                        <div className="col">
                        <input ref="address" type="text" className="form-control" placeholder="Address"/>
                        </div>
                        <div className="col">
                        <input ref="password" type="password" className="form-control" placeholder="Password"/>
                        </div>
                        <div className="col">
                        <button type="submit" className="btn btn-primary">Login</button>
                        </div>
                    </div>
                    <br/>

                </form>
            </div>
        );
    }
}

export default Login;