// Created By Bipul Wagle;
// AddMember Form Component

import React, { Component } from 'react';
import MemberStore from "../stores/MembersStore";
import * as MembersAction from "../actions/MemberActions";

class AddMember extends Component{

    //sets up function bindings and state.
    constructor(){
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.memberStoreOnCreate = this.memberStoreOnCreate.bind(this);
        this.state = {
        };
        
    }

    //adding and removing store listeners for component mount and dismount;
    componentWillMount(){
        // DONT DO THIS this.memberStoreOnChange.bind(this)
        MemberStore.on("create", this.memberStoreOnCreate);
    }

    componentWillUnmount(){
        MemberStore.removeListener("create", this.memberStoreOnCreate);
    }

    //checks if member create was successfull. resseting the form on success
    memberStoreOnCreate(data){
        if(!data || data.error){

        }else{
            this.refs.first_name.value = "";
            this.refs.last_name.value = "";
            this.refs.email.value = "";
            this.refs.gender.value = "";
            this.refs.age.value = null;
        }
    }

    //prevent form submission. Call User MemberAction to create member. Which inturn should change MemberStore.
    handleSubmit(e){
        e.preventDefault();
        var input_member = {
            first_name: this.refs.first_name.value,
            last_name: this.refs.last_name.value,
            email: this.refs.email.value,
            gender: this.refs.gender.value,
            age: this.refs.age.value
        }
        MembersAction.createMembers(input_member);
    }

    

    render(){
        return(
        <div className="AddMember">

            <br/>
            <div className="title">Add Member</div>

            <form onSubmit={this.handleSubmit}>
                    <div className="row">
                        <div className="col">
                        <input ref="first_name" type="text" className="form-control" placeholder="First Name"/>
                        </div>
                        <div className="col">
                        <input ref="last_name" type="text" className="form-control" placeholder="Last Name"/>
                        </div>
                    </div>
                    <br/>
                    <div className="row"> 
                        <div className="col-7">
                        <input ref="email" type="email" name="email" className="form-control" placeholder="Email" />
                        </div>
                        <div className="col">
                            <input ref="gender" type="text" name="gender" className="form-control" placeholder="Gender" />
                        </div>
                        <div className="col">
                            <input ref="age" type="number" name="age" className="form-control" placeholder="Age" />
                        </div>
                    </div>
                    <br/>
                    <div className="row">
                        <button type="submit" className="btn btn-primary">Add</button>
                        {/* <p><input type="submit" value="Add" /></p> */}
                    </div>
            </form>

            
        </div>
        );
    }
}

export default AddMember;