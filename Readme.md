Installation Instructions!
==========================

create-react-app installation
-----------------------------

    npm install -g create-react-app


Setup
-----

Clone the Repo

    git clone https://bitbucket.org/bipulwagle/fsl-react-webapp


    cd fsl-react-webapp

Install Dependencies

    npm install --save

Start Server

    npm start

server should be serving, please visit http://localhost:8080


Backend Endpoint Configuration
------------------------------
In /src/lib/NetworkConfig.js

    backendEndpoint = "NEW_ENDPOINT_ADDRESS";

